# PostgreSQL

PostgreSQL master/replica/barman backup pilot automation with Ansible

In this simple example we provision up to 3 hosts:
  1) PostgreSQL master node
  2) PostgreSQL replica node
  3) Barman backup node

We control with flags if we want to provision replica and/or barman backup.

Notes:

Since barman requires some ssh keys to be provisioned between barman server
and PostgreSQL master node, we handle some ssh-keys provisioning as part of
current roles here.
The idea is to create proper ssh-keys role which can handle everything related
to ssh keys and known_hosts.

We don't handle known_hosts files at the moment. Adding keys in known_hosts
is manual at the moment. Remember to ssh from barman host to pgsql master and
vice-versa to verify ssh connection after automation (also to get known_hosts
in place). This task is planned as separate ssh-keys role (TBD).

We aim to define all relevant variables in group_vars/postgres-servers/main.yml
Secrets are vaults in vault.yml file (as well as private ssh-keys)

This pilot is tested on Ubuntu 20 and presumes some basic packages to be already
inplace, along with ntpd and syslog configured.
This is done in my environment with common role which assures basic setup before
any other automation.
Common role is specific to my home setup and is out of scope of this pilot.

Files to be vaulted (in this repo are set as non-vaulted for easier understanding):
- group_vars/postgres-servers/vault.yml
- roles/postgresql-master/templates/ssh-keys/postgres/id_rsa.j2
- roles/postgresql-barman/templates/ssh-keys/barman/id_rsa.j2

## Inventory

Automation expects following Ansible inventory groups and hosts

```
[postgres-servers:children]
postgres-master
postgres-replica
postgres-barman

[postgres-master]
postgresql_master_node_here

[postgres-replica]
postgresql_replica_node_here

[postgres-barman]
postgresql_barman_node_here
```


## Roles and playbooks

We operate with 3 roles and  1 playbook.
Roles:
  - postgresql-master
  - postgresql-replica
  - postgresql-barman

Playbook: postgres-cluster.yml

## Variables

### Important flags

Flag to determinate if we wanna provision PostgreSQL replica

    flg_replica

Flag to determinate if we wanna provision PostgreSQL barman backup

    flg_barman

### PostgreSQL master / replica / barman hosts

Definition of PostgreSQL master/replica/barman nodes (preferably FQDN)

    pgsql_master_host
    pgsql_replica_host
    barman_host

### PostgreSQL common variables

PostgreSQL packages to be installed

    postgres_pkgs

PostgreSQL user and group (OS level)

    postgres_user
    postgres_group

PostgreSQL data directory

    postgres_datadir

PostgreSQL home directory

    postgres_user_home

PostgreSQL conf file

    postgres_conf_file

### PostgreSQL master specific variables

PostgreSQL conf file changes needed to enable WAL and sending them to barman backup.
We define list of options in form: option/value.

    pgsql_barman_conf_options

Example:
```
pgsql_barman_conf_options:
  - option: "wal_level"
    value: "archive"
  - option: "archive_mode"
    value: "on"
  - option: "archive_command"
    value: "rsync -a %p {{ barman_user }}@{{ barman_host }}:{{ barman_datadir }}/{{ pgsql_master_host }}/incoming/%f"
```

### PostgreSQL replica specific variables

Database user  and password for replication (DB level)
Note that password will be vaulted  with vault_ prefix.

    replication_dbuser
    replication_dbpassword

### Barman specific variables  

Barman packages to be installed

    barman_pkgs

Barman user and group (OS level)

    barman_user
    barman_group

Database user  and password for barman backup (DB level)
Note that password will be vaulted  with vault_ prefix.

    barman_dbuser
    barman_dbpassword

Barman datadir

    barman_datadir

Barman home directory

    barman_user_home

Barman hosts to be added to backup
List of dictionaries with specs for each host to be added to barman backup.
Retention and cron pattern definitions can vary.
Retention is self explanatory.
Cron pattern defines when full backup is to be taken.
Example:

```
barman_pgsql_hosts:
  pgsqlmaster:
    pgsqlhost: "{{ pgsql_master_host }}"
    description: "pgsqlm DB server"
    pgsql_barman_user: "{{ barman_dbuser }}"
    pgsql_barman_passwd: "{{ barman_dbpassword }}"
    pgsql_admin_user: "postgres"
    pgsql_dbname: "postgres"
    retention: "RECOVERY WINDOW OF 3 DAYS"
    cron: "15 03 * * *"
```

### SSH keys specific vatriables 

These are scheduled to me moved to separate ssh-keys role, as well as the ssh-key related
logic

SSH public keys to be provisioned.
Defines server, user on server and ssh-key to be pushed in authorized_keys on user@server

    ssh_auth_keys

## Usage

```
ansible-playbook postgres-cluster.yml -D
```

